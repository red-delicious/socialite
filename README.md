# SOCIALITE

## Red Delicious Team

-   Nick Kairui Zheng
-   Burcu Kantar Natoli
-   Jeff Montag

## Install Extensions

-   Git Graph: https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph

## Design

![Img](/images/design.png)

-   Fast API
-   PostgreSQL
-   React to build Single-Page Application (SPA)

## Socialite - Project Initialization

1-Star with cloning repository onto local computers
2-Navigate to project directory created from cloning repository
3-Create the docker database_volume

```
docker volume create database_volume

```

4-Build the docker containers.

```
docker compose build

```

5-Run the docker containers, make sure all containers are running

```
docker compose up

```

6- Access to the Socialite App -> http://localhost:5173/
![Img](/images/welcome.png)

7- Access the FastAPI web docs -> http://localhost:8000/docs#/
![Img](/images/FastAPi.png)

8- API documentation Endpoint -> https://docs.google.com/document/d/1Tw4v4jGr5g0lAv6PXOTVFhmy5AEMXVzNQI02vH734E0/edit

## Functionality

-   Users need to sign up and sign in for to be able to create an events.
-   Users can view all events created by all his/her friends.
-   Users can create an event as a host.
-   Users can search for other users and send a friend request to other users.
-   Users can accept or deny friend requests.
-   Users can send an event invitation to a friend.
-   Users can accept or deny the event invitations.
-   Users can see detail of events.
-   User can see her/his friends and visit their profiles to see his/her created events and joined events.
-   Users can make changes on created events such as, event name, event type, date of event, capacity or location.
