steps = [
    [
        """
        ALTER TABLE samples
        ADD user_id integer NOT NULL DEFAULT(1) REFERENCES users(id);
        """,
        """
        DROP COLUMN user_id;
        """,
    ],
]
