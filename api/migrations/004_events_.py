steps = [
    [
        """
        CREATE TABLE events (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL,
            description TEXT,
            creator_id INT NOT NULL REFERENCES users (id),
            date DATE NOT NULL,
            type VARCHAR(100) NOT NULL,
            capacity INT,
            location VARCHAR(250) NOT NULL
        );
        """,
        """
        DROP TABLE events;
        """,
    ],
]
