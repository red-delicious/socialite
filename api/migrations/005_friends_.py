steps = [
    [
        """
        CREATE TABLE friends (
            id SERIAL PRIMARY KEY NOT NULL,
            sender_id INT NOT NULL REFERENCES users (id),
            receiver_id INT NOT NULL REFERENCES users (id),
            status BOOL NOT NULL,
            UNIQUE (sender_id, receiver_id)
        )
        """,
        """
        DROP TABLE friends;
        """,
    ],
]
