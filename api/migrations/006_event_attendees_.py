steps = [
    [
        """
        CREATE TABLE event_attendees (
            id SERIAL PRIMARY KEY NOT NULL,
            event_id INT NOT NULL REFERENCES events (id),
            host_id INT NOT NULL REFERENCES users (id),
            attendee_id INT NOT NULL REFERENCES users (id),
            status VARCHAR(20) NOT NULL,
            UNIQUE (event_id, attendee_id)
        )
        """,
        """
        DROP TABLE event_attendees;
        """,
    ],
]
