from pydantic import BaseModel


class EventAttendee(BaseModel):
    event_id: int
    host_id: int
    attendee_id: int
    status: str


class EventAttendeeResponse(BaseModel):
    status: str
    event_id: int
    host_id: int
    host_username: str
    attendee_id: int
    attendee_username: str


class EventAttendeeRequest(BaseModel):
    attendee_id: int
    attendee_username: str


class InvitationResponse(BaseModel):
    event_id: int
    host_id: int
    username: str
