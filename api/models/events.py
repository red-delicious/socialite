"""
Pydantic Model for Events

any step in chain needs its own pydantic model
use routes to declare where userid is going to be pulled from
get userid from JWT and dictionary that comes from it to access user data
can have base "event" model that declares the inputs that user puts in
(location, etc) and then the response model will generate the auto properties
like id, userid

everything into and out of database will require a model in order for it to be
translated

"""

from pydantic import BaseModel
from datetime import date


class CreateEvent(BaseModel):
    name: str
    description: str
    date: date
    type: str
    capacity: int
    location: str


class CreateEventResponse(BaseModel):
    id: int
    name: str
    description: str
    creator_id: int
    date: date
    type: str
    capacity: int
    location: str


class DetailAndGetEventResponse(BaseModel):
    id: int
    name: str
    description: str
    creator_id: int
    creator_username: str
    date: date
    type: str
    capacity: int
    location: str


class DetailEvent(BaseModel):
    event_id: int
    name: str
    date: date
    type: str
    description: str
    location: str
    capacity: int


"""
class EventBase(BaseModel)

class Event (EventBase)
    userid


# routes will be where you tell where userID is inherting from

"""
