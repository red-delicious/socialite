"""
Pydantic Model for Friend

any step in chain needs its own pydantic model
use routes to declare where userid is going to be pulled from
get userid from JWT and dictionary that comes from it to access user data
can have base "friend" model that declares the inputs that user puts in
and then the response model will generate the auto properties
like id, userid, friend_id

everything into and out of database will require a model in order for it to be
translated

"""

from pydantic import BaseModel


class CreateFriend(BaseModel):
    sender_id: int
    receiver_id: int


class CreateFriendResponse(BaseModel):
    id: int
    sender_id: int
    receiver_id: int
    status: bool


class FriendRequest(BaseModel):
    sender_id: int
    username: str


class Friend(BaseModel):
    username: str
    friend_id: int


class UsersResponse(BaseModel):
    id: int
    username: str
