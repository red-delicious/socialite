import os
import psycopg
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
from models.event_attendees import EventAttendeeResponse, EventAttendee
from models.event_attendees import InvitationResponse
from utils.exceptions import EventAttendeeDataBaseException
from typing import List


DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class EventAttendeeQueries:
    def view_invitation_detail(self, event_id: int):
        """
        Gets all invitations from the database for a specific event

        Returns None if the invitation isn't found
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(
                    row_factory=class_row(EventAttendeeResponse)
                ) as cur:
                    cur.execute(
                        """
                            SELECT
                            event_attendees.status,
                            event_attendees.event_id,
                            event_attendees.host_id,
                            event_attendees.attendee_id,
                            attendee_users.username AS attendee_username,
                            host_users.username AS host_username
                            FROM event_attendees
                            INNER JOIN users as host_users ON
                            event_attendees.host_id = host_users.id
                            INNER JOIN users as attendee_users ON
                            event_attendees.attendee_id = attendee_users.id
                            WHERE event_attendees.event_id = %s;
                        """,
                        [event_id],
                    )
                    invitations = cur.fetchall()
                    if not invitations:
                        return []
        except psycopg.Error as e:
            print(e)
            raise EventAttendeeDataBaseException("Error getting invitations")
        return invitations

    def accept_invitation(self, user_id: int, friend_id: int, event_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(EventAttendee)) as cur:
                    cur.execute(
                        """
                        UPDATE event_attendees
                        SET status = %s
                        WHERE host_id = %s
                        and attendee_id = %s
                        and event_id = %s
                        RETURNING *;
                        """,
                        ["accepted", friend_id, user_id, event_id],
                    )
                    event_attendee = cur.fetchone()
                    if not event_attendee:
                        raise EventAttendeeDataBaseException(
                            "Could not accept invitation"
                        )
        except psycopg.Error:
            raise EventAttendeeDataBaseException(
                "Could not accept invitation. Incorrect data input"
            )
        return event_attendee

    def delete_attendee(self, user_id: int, event_id: int, attendee_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM event_attendees
                        WHERE event_id = %s and host_id = %s
                        and attendee_id = %s
                        RETURNING *;
                        """,
                        [event_id, user_id, attendee_id],
                    )
                    event = cur.fetchone()
                    if not event:
                        raise EventAttendeeDataBaseException(
                            "Already deleted."
                        )
        except psycopg.Error:
            raise EventAttendeeDataBaseException("Could not delete event.")
        return event

    def invite_friend_to_event(
        self, user_id: int, event_id: int, friend_id: int
    ):
        """
        Invites a friend to an event

        Raises:
            EventAttendeeDataBaseException:
            If an error occurs while inviting the friend
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(EventAttendee)) as cur:
                    cur.execute(
                        """
                        INSERT INTO event_attendees (
                            event_id, host_id, attendee_id, status)
                        VALUES (%s, %s, %s, %s)
                        RETURNING *;
                        """,
                        [event_id, user_id, friend_id, "pending"],
                    )
                    invitation = cur.fetchone()
                    if not invitation:
                        raise EventAttendeeDataBaseException(
                            "Could not invite friend to an event."
                        )
        except psycopg.Error as e:
            print(e)
            raise EventAttendeeDataBaseException(
                "Error! inviting friend to event"
            )
        return invitation

    def view_all_invitations(self, user_id: int) -> List[InvitationResponse]:
        """
        Gets all invitations from the database

        Returns None if the invitaions isn't found
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(
                    row_factory=class_row(InvitationResponse)
                ) as cur:
                    cur.execute(
                        """
                            SELECT
                                event_Attendees.event_id as event_id,
                                event_Attendees.host_id as host_id,
                                users.username as username
                            FROM users
                            LEFT JOIN event_Attendees
                            ON users.id = event_Attendees.host_id
                            WHERE event_Attendees.attendee_id = %s
                                  and event_Attendees.status = %s;
                        """,
                        [
                            user_id,
                            "pending",
                        ],
                    )
                    invitations = cur.fetchall()
                    if not invitations:
                        return []
        except psycopg.Error as e:
            print(e)
            raise EventAttendeeDataBaseException("Error getting invitations")
        return invitations

    def deny_invitation(self, user_id: int, friend_id: int, event_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        UPDATE event_attendees
                        SET status = %s
                        WHERE host_id = %s
                        and attendee_id = %s
                        and event_id = %s;
                        """,
                        ["denied", friend_id, user_id, event_id],
                    )
                    if cur.rowcount == 0:
                        raise EventAttendeeDataBaseException("No invitation")
        except psycopg.Error as e:
            print(f"Database error occurred: {e}")
            raise EventAttendeeDataBaseException(
                "Could not deny invitation. Incorrect data input"
            )

    def view_all_accepted_invitations(
        self, user_id: int
    ) -> List[InvitationResponse]:
        """
        Gets all invitations from the database

        Returns None if the invitaions isn't found
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(
                    row_factory=class_row(InvitationResponse)
                ) as cur:
                    cur.execute(
                        """
                            SELECT
                                event_Attendees.event_id as event_id,
                                event_Attendees.host_id as host_id,
                                users.username as username
                            FROM users
                            LEFT JOIN event_Attendees
                            ON users.id = event_Attendees.host_id
                            WHERE event_Attendees.attendee_id = %s
                                  and event_Attendees.status = %s;
                        """,
                        [
                            user_id,
                            "accepted",
                        ],
                    )
                    invitations = cur.fetchall()
                    if not invitations:
                        return []
        except psycopg.Error as e:
            print(e)
            raise EventAttendeeDataBaseException("Error getting invitations")
        return invitations
