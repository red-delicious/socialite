"""
Database Queries for Events
"""

import os
import psycopg
from typing import List
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
from models.events import CreateEvent, CreateEventResponse
from models.events import DetailAndGetEventResponse
from utils.exceptions import EventDatabaseException


DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class EventQueries:
    def create_event(self, event: CreateEvent, user_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor(
                    row_factory=class_row(CreateEventResponse)
                ) as cur:
                    cur.execute(
                        """
                        INSERT INTO events (
                            name,
                            description,
                            creator_id,
                            date,
                            type,
                            capacity,
                            location
                        ) VALUES (
                            %s, %s, %s, %s, %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        [
                            event.name,
                            event.description,
                            user_id,
                            event.date,
                            event.type,
                            event.capacity,
                            event.location,
                        ],
                    )
                    event = cur.fetchone()
                    if not event:
                        raise EventDatabaseException(
                            "Could not create event. Incorrect data input"
                        )
        except psycopg.Error:
            raise EventDatabaseException(
                "Could not create event. Incorrect data input"
            )
        return event

    def edit_event(self, event: CreateEvent, user_id: int, event_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor(
                    row_factory=class_row(CreateEventResponse)
                ) as cur:
                    cur.execute(
                        """
                        UPDATE events
                    SET name = %s,
                        description = %s,
                        creator_id = %s,
                        date = %s,
                        type = %s,
                        capacity = %s,
                        location = %s
                    WHERE id = %s
                    RETURNING *;
                        """,
                        [
                            event.name,
                            event.description,
                            user_id,
                            event.date,
                            event.type,
                            event.capacity,
                            event.location,
                            event_id,
                        ],
                    )
                    event = cur.fetchone()
                    if not event:
                        raise EventDatabaseException(
                            "Could not update event. Incorrect data input"
                        )
        except psycopg.Error:
            raise EventDatabaseException(
                "Could not update event. Incorrect data input"
            )
        return event

    def detail_event(self, event_id: int) -> DetailAndGetEventResponse:
        try:
            with pool.connection() as conn:
                with conn.cursor(
                    row_factory=class_row(DetailAndGetEventResponse)
                ) as cur:
                    cur.execute(
                        """
                        SELECT
                            events.id,
                            events.name,
                            events.description,
                            events.creator_id,
                            users.username as creator_username,
                            events.date,
                            events.type,
                            events.capacity,
                            events.location
                        FROM events
                        LEFT JOIN users
                        on events.creator_id = users.id
                        WHERE events.id=%s;
                        """,
                        [event_id],
                    )
                    event = cur.fetchone()
                    print(event)
                    if not event:
                        raise EventDatabaseException(
                            "Could not list event. Incorrect data input"
                        )
        except psycopg.Error:
            raise EventDatabaseException(
                "Could not list event. Incorrect data input"
            )
        return event

    def delete_event(self, user_id: int, event_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM events
                        WHERE id = %s and creator_id = %s
                        RETURNING *;
                        """,
                        [event_id, user_id],
                    )
                    event = cur.fetchone()
                    if not event:
                        raise EventDatabaseException("Already deleted.")
        except psycopg.Error:
            raise EventDatabaseException("Could not delete event.")
        return event

    def get_events(self) -> List[DetailAndGetEventResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor(
                    row_factory=class_row(DetailAndGetEventResponse)
                ) as cur:
                    cur.execute(
                        """
                        SELECT
                            events.id,
                            events.name,
                            events.description,
                            events.creator_id,
                            users.username as creator_username,
                            events.date,
                            events.type,
                            events.capacity,
                            events.location
                        FROM events
                        LEFT JOIN users
                        on events.creator_id = users.id
                        """
                    )
                    events = cur.fetchall()
                    if not events:
                        raise EventDatabaseException("No events posted.")
        except psycopg.Error:
            raise EventDatabaseException("Error getting data")
        return events
