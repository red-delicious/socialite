"""
Database Queries for Friends
"""

import os
from typing import List
import psycopg
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
from models.friends import (
    FriendRequest,
    CreateFriendResponse,
    Friend,
    UsersResponse,
)
from utils.exceptions import FriendDatabaseException

# Connect to Database
DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool = ConnectionPool(DATABASE_URL)


class FriendQueries:

    def view_friends(self, user_id: int) -> List[Friend]:
        """
        Views all a users friends
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(Friend)) as cur:
                    cur.execute(
                        """
                        select users.username, f.friend_id
                        from
                        (SELECT
                            case when sender_id = %s then receiver_id
                            when receiver_id = %s then sender_id
                            end as friend_id
                        FROM friends
                        WHERE (receiver_id = %s or sender_id = %s)
                        and status = %s) as f
                        left join users
                        on f.friend_id = users.id
                        """,
                        [
                            user_id,
                            user_id,
                            user_id,
                            user_id,
                            True,
                        ],
                    )
                    friends = cur.fetchall()
                    print(friends)
                    if not friends:
                        raise FriendDatabaseException("No friends")
        except psycopg.Error as e:
            print(e)
            raise FriendDatabaseException("Error getting requests")
        return friends

    def view_requests(self, user_id: int) -> List[FriendRequest]:
        """
        Gets all user requests from the database

        Returns None if the user isn't found
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(FriendRequest)) as cur:
                    cur.execute(
                        """
                            SELECT
                                friends.sender_id,
                                users.username
                            FROM users
                            LEFT JOIN friends
                            on users.id = friends.sender_id
                            WHERE friends.receiver_id = %s
                                  and friends.status = %s;
                        """,
                        [
                            user_id,
                            False,
                        ],
                    )
                    requests = cur.fetchall()
                    if not requests:
                        return []
        except psycopg.Error as e:
            print(e)
            raise FriendDatabaseException("Error getting requests")
        return requests

    def send_friend_request(self, user_id: int, friend_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor(
                    row_factory=class_row(CreateFriendResponse)
                ) as cur:
                    cur.execute(
                        """
                        INSERT INTO friends (
                            sender_id,
                            receiver_id,
                            status
                        ) VALUES (
                            %s, %s, %s
                        )
                        RETURNING *;
                        """,
                        [user_id, friend_id, False],
                    )
                    friend = cur.fetchone()
                    if not friend:
                        raise FriendDatabaseException(
                            "Could not create a friend."
                        )
        except psycopg.Error:
            raise FriendDatabaseException(
                "You've already sent a friend request to this user"
            )
        return friend

    def delete_friend(self, user_id: int, friend_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM friends
                        WHERE sender_id = %s and receiver_id = %s
                        RETURNING *;
                        """,
                        [friend_id, user_id],
                    )
                    friend = cur.fetchone()
                    if not friend:
                        raise FriendDatabaseException()
        except psycopg.Error:
            raise FriendDatabaseException("Could not remove your friend.")

    def accept_friend(self, user_id: int, friend_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor(
                    row_factory=class_row(CreateFriendResponse)
                ) as cur:
                    cur.execute(
                        """
                        UPDATE friends
                        SET status = %s
                        WHERE sender_id = %s and receiver_id = %s
                        RETURNING *;
                        """,
                        [
                            True,
                            friend_id,
                            user_id,
                        ],
                    )
                    friend = cur.fetchone()
                    if not friend:
                        raise FriendDatabaseException(
                            "Could not accept friend."
                        )
        except psycopg.Error:
            raise FriendDatabaseException(
                "Could not accept new friend. Incorrect data input"
            )
        return friend

    def view_all_users(self) -> List[UsersResponse]:
        """
        Views all a users
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(UsersResponse)) as cur:
                    cur.execute(
                        """
                        select users.id, users.username
                        from
                        users
                        """,
                        [],
                    )
                    users = cur.fetchall()
                    if not users:
                        raise FriendDatabaseException("No users")
        except psycopg.Error:
            raise FriendDatabaseException("Error getting requests")
        return users
