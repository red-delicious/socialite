"""
Events Router

JWTToken from jwt.py in models has user id information
"""

from fastapi import (
    Depends,
    Request,
    Response,
    HTTPException,
    status,
    APIRouter,
)


from queries.event_attendees_queries import (
    EventAttendeeQueries,
)
from models.jwt import JWTUserData
from utils.authentication import try_get_jwt_user_data

router = APIRouter(tags=["EventAttendees"], prefix="/api/user")


@router.get("/{user_id}/events/{event_id}/invitations")
async def view_invitation_detail(
    user_id: int,
    event_id: int,
    request: Request,
    response: Response,
    queries: EventAttendeeQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user.id == user_id:
        try:
            invitations = queries.view_invitation_detail(event_id)
            return invitations
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User is not authorized.",
        )


@router.put("/{user_id}/friends/{friend_id}/events/{event_id}/accept")
async def accept_invitation(
    user_id: int,
    friend_id: int,
    event_id: int,
    request: Request,
    response: Response,
    queries: EventAttendeeQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user.id == user_id:
        try:
            accept = queries.accept_invitation(user_id, friend_id, event_id)
            return accept
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise ValueError("You are not invited to this event")


@router.delete("/{user_id}/events/{event_id}/{attendee_id}")
async def delete_attendee(
    user_id: int,
    event_id: int,
    attendee_id: int,
    request: Request,
    response: Response,
    queries: EventAttendeeQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user:
        try:
            queries.delete_attendee(user_id, event_id, attendee_id)
            return {"Successfully deleted"}
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You are not authorized to delete this event",
        )


@router.post("/{user_id}/events/{event_id}/friends/{friend_id}/invite")
async def invite_friend_to_event(
    user_id: int,
    event_id: int,
    friend_id: int,
    request: Request,
    response: Response,
    queries: EventAttendeeQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user.id == user_id:
        try:
            new_invitation = queries.invite_friend_to_event(
                user_id, event_id, friend_id
            )
            return new_invitation
        except Exception as e:
            raise HTTPException(status_code=404, detail=str(e))
    else:
        raise ValueError("User not found")


@router.get("/{user_id}/invitations")
async def view_all_invitations(
    user_id: int,
    request: Request,
    response: Response,
    queries: EventAttendeeQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user:
        try:
            invitations = queries.view_all_invitations(user_id)
            return invitations
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise ValueError("you should sign in")


@router.put("/{user_id}/friends/{friend_id}/events/{event_id}/deny")
async def deny_invitation(
    user_id: int,
    friend_id: int,
    event_id: int,
    request: Request,
    response: Response,
    queries: EventAttendeeQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user:
        try:
            queries.deny_invitation(user_id, friend_id, event_id)
            return {"denied successfully"}
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise ValueError("You are not signed in")


@router.get("/{user_id}/invitations/accepted")
async def view_all_accetped_invitations(
    user_id: int,
    request: Request,
    response: Response,
    queries: EventAttendeeQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user:
        try:
            invitations = queries.view_all_accepted_invitations(user_id)
            return invitations
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise ValueError("you should sign in")
