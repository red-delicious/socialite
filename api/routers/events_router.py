"""
Events Router

JWTToken from jwt.py in models has user id information
"""

from fastapi import (
    Depends,
    Request,
    Response,
    HTTPException,
    status,
    APIRouter,
)


from queries.event_queries import (
    EventQueries,
)

from models.events import CreateEvent
from models.jwt import JWTUserData
from utils.authentication import try_get_jwt_user_data

router = APIRouter(tags=["Events"], prefix="/api/events/user")


@router.post("/{user_id}/events")
async def create_event(
    events: CreateEvent,
    user_id: int,
    request: Request,
    response: Response,
    queries: EventQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user.id == user_id:
        try:
            new_event = queries.create_event(events, user_id)
            return new_event
        except Exception as e:
            raise
            HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise ValueError("you are not the creator of the event")


@router.put("/{user_id}/events/{event_id}")
async def edit_event(
    events: CreateEvent,
    user_id: int,
    event_id: int,
    request: Request,
    response: Response,
    queries: EventQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user.id == user_id:
        try:
            event = queries.edit_event(events, user_id, event_id)
            return event
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise ValueError("you should sign in")


@router.get("/{user_id}/events/{event_id}")
async def detail_event(
    user_id: int,
    event_id: int,
    request: Request,
    response: Response,
    queries: EventQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user.id == user_id:
        event = queries.detail_event(event_id)
        if event.id == event_id:
            return event
        else:
            raise HTTPException(status_code=404, detail="Event not found")
    else:
        raise ValueError("you are not the creator of the event")


@router.delete("/{user_id}/events/{event_id}")
async def delete_event(
    user_id: int,
    event_id: int,
    request: Request,
    response: Response,
    queries: EventQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user:
        try:
            queries.delete_event(user_id, event_id)
            return {"Successfully deleted"}
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You are not authorized to delete this event",
        )


@router.get("/events")
async def get_events(
    request: Request,
    response: Response,
    queries: EventQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user:
        try:
            events = queries.get_events()
            return events
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise ValueError("You are not logged in")
