"""
Friends Router

JWTToken from jwt.py in models has user id information
"""

from fastapi import (
    Depends,
    Request,
    Response,
    HTTPException,
    status,
    APIRouter,
)


from queries.friend_queries import (
    FriendQueries,
)
from models.jwt import JWTUserData
from utils.authentication import try_get_jwt_user_data

router = APIRouter(tags=["Friends"], prefix="/api/user")


@router.post("/{user_id}/friends/{friend_id}/request")
async def send_friend_request(
    user_id: int,
    friend_id: int,
    request: Request,
    response: Response,
    queries: FriendQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user.id == user_id:
        try:
            new_friend = queries.send_friend_request(user_id, friend_id)
            return new_friend
        except Exception as e:
            raise HTTPException(status_code=404, detail=str(e))
    else:
        raise ValueError("User not found")


@router.put("/{user_id}/friends/{friend_id}/accept")
async def accept_friend(
    user_id: int,
    friend_id: int,
    request: Request,
    response: Response,
    queries: FriendQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user.id == user_id:
        try:
            accept = queries.accept_friend(user_id, friend_id)
            return accept
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise ValueError("you are not a friend with user")


@router.delete("/{user_id}/friends/{friend_id}")
async def delete_friend(
    user_id: int,
    friend_id: int,
    request: Request,
    response: Response,
    queries: FriendQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user.id == user_id:
        try:
            queries.delete_friend(user_id, friend_id)
            return {"Your friend has been removed from the list"}
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You are not authorized to remove a friend",
        )


@router.get("/{user_id}/requests", response_model=None)
async def view_requests(
    user_id: int,
    request: Request,
    response: Response,
    queries: FriendQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user:
        try:
            requests = queries.view_requests(user_id)
            return requests
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise ValueError("you should sign in")


@router.get("/{user_id}/friends")
async def view_friends(
    user_id: int,
    response: Response,
    request: Request,
    queries: FriendQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user:
        try:
            requests = queries.view_friends(user_id)
            return requests
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )

    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)


@router.get("/all")
async def view_all_users(
    response: Response,
    request: Request,
    queries: FriendQueries = Depends(),
    user: JWTUserData = Depends(try_get_jwt_user_data),
):
    if user:
        try:
            users = queries.view_all_users()
            return users
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail=str(e)
            )
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
