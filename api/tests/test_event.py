from main import app
from fastapi.testclient import TestClient
from queries.event_queries import EventQueries
from models.events import CreateEventResponse

from models.users import UserResponse

from utils.authentication import try_get_jwt_user_data

client = TestClient(app)


def JwtUserData():
    return UserResponse(id=1, username="name")


class EmptyEventQueries:
    def list_events(self):
        return []


class CreateEventQueries:
    def create_event(self, event: CreateEventResponse, user_id: int):
        event = CreateEventResponse(
            id=1,
            creator_id=user_id,
            name=event.name,
            date=event.date,
            type=event.type,
            description=event.description,
            location=event.location,
            capacity=event.capacity,
        )
        return event


def test_create_event():
    app.dependency_overrides[EventQueries] = CreateEventQueries
    app.dependency_overrides[try_get_jwt_user_data] = JwtUserData

    json_data = {
        "name": "string",
        "description": "string",
        "date": "2024-06-12",
        "type": "string",
        "capacity": 0,
        "location": "string",
    }
    expected = {
        "id": 1,
        "name": "string",
        "description": "string",
        "creator_id": 1,
        "date": "2024-06-12",
        "type": "string",
        "capacity": 0,
        "location": "string",
    }

    response = client.post("/api/events/user/1/events", json=json_data)
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected


def test_get_all_events():
    app.dependency_overrides[EventQueries] = CreateEventQueries
    app.dependency_overrides[try_get_jwt_user_data] = JwtUserData

    response = client.get("/api/events")
    app.dependency_overrides = {}

    assert response.status_code == 404
    assert response.json() == {"detail": "Not Found"}
