from main import app
from fastapi.testclient import TestClient
from queries.event_attendees_queries import EventAttendeeQueries
from models.users import UserResponse
from models.event_attendees import InvitationResponse

from utils.authentication import try_get_jwt_user_data

client = TestClient(app)


def JwtUserData():
    return UserResponse(id=1, username="name")


class FakeEventAttendeeQueries:
    def invite_friend_to_event(
        self, host_id: int, event_id: int, attendee_id: int
    ):
        return {
            "host_id": host_id,
            "event_id": event_id,
            "attendee_id": attendee_id,
            "status": "pending",
        }

    def view_all_invitations(self, user_id: int):
        return [
            InvitationResponse(event_id=1, host_id=user_id, username="string")
        ]


def test_invite_friend_to_event():

    app.dependency_overrides[try_get_jwt_user_data] = JwtUserData
    app.dependency_overrides[EventAttendeeQueries] = FakeEventAttendeeQueries

    response = client.post("/api/user/1/events/1/friends/2/invite")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
        "event_id": 1,
        "host_id": 1,
        "attendee_id": 2,
        "status": "pending",
    }


def test_view_event_invitation():

    app.dependency_overrides[try_get_jwt_user_data] = JwtUserData
    app.dependency_overrides[EventAttendeeQueries] = FakeEventAttendeeQueries

    response = client.get("/api/user/1/invitations")

    app.dependency_overrides = {}

    expected_response = [
        {
            "event_id": 1,
            "host_id": 1,
            "username": "string",
        }
    ]
    assert response.status_code == 200
    assert response.json() == expected_response
