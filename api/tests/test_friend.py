from main import app
from fastapi.testclient import TestClient
from queries.friend_queries import FriendQueries
from models.friends import FriendRequest
from models.users import UserResponse

from utils.authentication import try_get_jwt_user_data

client = TestClient(app)


def JwtUserData():
    return UserResponse(id=1, username="name")


class FakeFriendQueries:
    def send_friend_request(self, sender_id: int, receiver_id: int):
        return {
            "sender_id": sender_id,
            "receiver_id": receiver_id,
            "status": False,
        }

    def view_requests(self, user_id: int):
        return [FriendRequest(sender_id=user_id, username="John")]


def test_send_friend_request():
    # Arrange
    app.dependency_overrides[try_get_jwt_user_data] = JwtUserData
    app.dependency_overrides[FriendQueries] = FakeFriendQueries

    # Act
    response = client.post("/api/user/1/friends/2/request")

    # Clean up
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == {
        "sender_id": 1,
        "receiver_id": 2,
        "status": False,
    }


def test_view_requests():
    # Arrange
    app.dependency_overrides[try_get_jwt_user_data] = JwtUserData
    app.dependency_overrides[FriendQueries] = FakeFriendQueries

    # Act
    response = client.get("/api/user/1/requests")

    # Clean up
    app.dependency_overrides = {}

    # Assert
    expected_response = [{"sender_id": 1, "username": "John"}]
    assert response.status_code == 200
    assert response.json() == expected_response
