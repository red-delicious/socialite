"""
Custom Exceptions for the App
"""


class UserDatabaseException(Exception):
    pass


class EventDatabaseException(Exception):
    pass


class FriendRequestException(Exception):
    pass


class FriendDatabaseException(Exception):
    pass


class EventAttendeeDataBaseException(Exception):
    pass
