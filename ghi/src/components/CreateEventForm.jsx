import useAuthService from './../hooks/useAuthService'
import { useState } from 'react'
import { Link } from 'react-router-dom'

function CreateEventForm() {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [date, setDate] = useState('')
    const [type, setType] = useState('')
    const [capacity, SetCapacity] = useState('')
    const [location, setLocation] = useState('')
    const { user } = useAuthService()

    async function handleFormSubmit(event) {
        event.preventDefault()
        const data = {}
        ;(data.name = name),
            (data.description = description),
            (data.date = date),
            (data.type = type),
            (data.capacity = capacity),
            (data.location = location)

        const eventUrl = `http://localhost:8000/api/events/user/${user.id}/events`
        const fetchConfig = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        }

        const response = await fetch(eventUrl, fetchConfig)
        if (response.ok) {
            setName('')
            setDescription('')
            setDate('')
            setType('')
            SetCapacity('')
            setLocation('')
        } else {
            console.error('Can not create an event')
        }
    }

    return (
        <form onSubmit={handleFormSubmit}>
            <div className="row">
                <div className="offset-3 col-6">
                    <h1> Create an Event </h1>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            placeholder="Event Name"
                            style={{ color: 'black' }}
                            required
                        />
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                            placeholder="Event Content"
                            style={{ color: 'black' }}
                            required
                        />
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            value={date}
                            onChange={(e) => setDate(e.target.value)}
                            placeholder="Date of an event"
                            style={{ color: 'black' }}
                        />
                    </div>
                    <div className="form-floating mb-3">
                        <select
                            value={type}
                            onChange={(e) => setType(e.target.value)}
                            required
                            className="form-select"
                        >
                            <option value=" ">Select Event Type</option>
                            <option value="Sport">Sport Events</option>
                            <option value="Music">Music Events</option>
                            <option value="Workshop">Workshops</option>
                            <option value="Social Gathering">Gatherings</option>
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            value={capacity}
                            onChange={(e) => SetCapacity(e.target.value)}
                            placeholder="Event Capacity"
                            style={{ color: 'black' }}
                        />
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            value={location}
                            onChange={(e) => setLocation(e.target.value)}
                            placeholder="Event Location"
                            style={{ color: 'black' }}
                        />
                    </div>
                    <button className="brown-button"> Create </button>
                    <Link to={`/invitefriend`}>
                        <button className="bg-yellow-700 text-white text-sm font-semibold rounded-full px-2 py-2 hover:bg-yellow-600 transition duration-300 ease-in-out;">
                            Invite Friend
                        </button>
                    </Link>
                </div>
            </div>
        </form>
    )
}
export default CreateEventForm
