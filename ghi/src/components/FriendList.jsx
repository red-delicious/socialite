import { useState, useEffect } from 'react'
import useAuthService from './../hooks/useAuthService'
import { Link } from 'react-router-dom'

export default function FriendList() {
    const [FriendList, setFriendList] = useState([])
    const { user } = useAuthService()

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(
                `http://localhost:8000/api/user/${user.id}/friends`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response.ok) {
                const data = await response.json()
                setFriendList(data)
            }
        }
        fetchData()
    }, [user.id])

    return (
        <>
            <h1 className="flex justify-center px-24 text-yellow-800">My Friend List</h1>
            <div className="m-3 flex justify-items-center px-44">
            <table className="table table-striped ml-auto text-yellow-800">
                <thead>
                    <tr>
                        <th>My Friends</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody>
                    {FriendList?.map((user) => {
                        return (
                            <tr key={user.friend_id}>
                                <td>{user.username}</td>
                                <td>
                                    <Link
                                        to={`/friendprofile/${user.friend_id}`}
                                    >
                                        <button className="brown-button">
                                            → Go to Profile
                                        </button>
                                    </Link>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            </div>
        </>
    )
}
