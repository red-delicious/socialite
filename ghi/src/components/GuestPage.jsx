import { useNavigate } from 'react-router-dom'

function GuestPage() {
    const navigate = useNavigate()
    const handleSignIn = () => {
        navigate('/signin') // Navigate to the signin page
    }
    const handleSignUp = () => {
        navigate('/signup') // Navigate to the signin page
    }

    return (
        <div className="max-w-7xl mx-auto px-4 py-8">
            <h1 className="text-2xl font-bold text-center mb-6">
                Welcome to Socialite!
            </h1>
            <div className="text-center">
                <button
                    onClick={handleSignIn}
                    className="bg-yellow-800 text-white px-4 py-2 rounded hover:text-yellow-400"
                >
                    Sign in
                </button>
                <button
                    onClick={handleSignUp}
                    className="bg-yellow-800 text-white px-4 py-2 rounded hover:text-yellow-400"
                >
                    Sign Up
                </button>
            </div>
        </div>
    )
}
export default GuestPage
