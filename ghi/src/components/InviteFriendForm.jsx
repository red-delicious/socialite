import useAuthService from './../hooks/useAuthService'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const InviteFriendForm = () => {
    const [friendName, setFriendName] = useState('')
    const [eventList, setEvents] = useState([])
    const [friendList, setFriendList] = useState([])
    const [eventDetails, setEventDetails] = useState('')
    const { user } = useAuthService()
    const navigate = useNavigate()

    const handleFriendNameChange = (e) => {
        setFriendName(e.target.value)
    }

    const handleEventDetailsChange = (e) => {
        setEventDetails(e.target.value)
    }

    async function handleSubmit(e) {
        e.preventDefault()
        const data = {}
        data.id = friendName
        const friendUrl = `http://localhost:8000/api/user/${user.id}/events/${eventDetails}/friends/${friendName}/invite`
        const fetchConfig = {
            method: 'post',
            credentials: 'include',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(friendUrl, fetchConfig)
        if (response.ok) {
            navigate('/myprofile')
        }
    }
    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(
                `http://localhost:8000/api/user/${user.id}/friends`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response.ok) {
                const data = await response.json()
                setFriendList(data)
            }
            const response2 = await fetch(
                `http://localhost:8000/api/events/user/events`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response2.ok) {
                const data = await response2.json()
                setEvents(data)
            }
        }
        fetchData()
    }, [user.id])

    return (
        <form onSubmit={handleSubmit}>
            <label>
                My Friends:
                <select
                    value={friendName}
                    onChange={handleFriendNameChange}
                    style={{ color: 'black' }}
                    required
                >
                    <option value="">Select friends</option>
                    {friendList.map((friend) => (
                        <option key={friend.friend_id} value={friend.friend_id}>
                            {friend.username}
                        </option>
                    ))}
                </select>
            </label>
            <br />
            <label>
                Events:
                <select
                    value={eventDetails}
                    onChange={handleEventDetailsChange}
                    style={{ color: 'black' }}
                    required
                >
                    <option value="">Select Event</option>
                    {eventList.map((event) => (
                        <option key={event.id} value={event.id}>
                            {event.name}
                        </option>
                    ))}
                </select>
            </label>
            <br />
            <button className="bg-yellow-700 text-white text-sm font-semibold rounded-full px-2 py-2 hover:bg-yellow-600 transition duration-300 ease-in-out;">
                {' '}
                Invite Friend{' '}
            </button>
        </form>
    )
}

export default InviteFriendForm
