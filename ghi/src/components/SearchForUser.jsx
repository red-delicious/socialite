// @ts-check
import { useState, useEffect } from 'react'
//import FriendRequestForm from './SendFriendRequest';
import { sendFriendRequest } from '../services/sendFriendRequest' // Adjust the path as necessary
import useAuthService from '../hooks/useAuthService'

export default function SearchForUser() {
    const [query, setQuery] = useState('')
    const [usersList, setUsersList] = useState([])
    const [selectedUser, setSelectedUser] = useState([])
    const { user } = useAuthService()

    const getUsers = async () => {
        const response = await fetch(`http://localhost:8000/api/user/all`, {
            method: 'GET',
            credentials: 'include',
        })
        if (response.ok) {
            const data = await response.json()
            setUsersList(data)
        }
    }

    const handleInputChange = (event) => {
        setQuery(event.target.value)
        setSelectedUser([])
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        for (let user of usersList) {
            if (user.username === query) {
                setSelectedUser([user.id, user.username])
            }
        }
    }

    useEffect(() => {
        getUsers()
    }, [])

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input
                    type="text"
                    value={query}
                    onChange={handleInputChange}
                    placeholder="Search for user.."
                    style={{ color: 'black' }}
                />
                <button type="submit">Search</button>
            </form>
            {selectedUser.length > 0 ? (
                <div>
                    <button
                        onClick={() =>
                            sendFriendRequest(user.id, selectedUser[0])
                        }
                    >
                        Add Friend
                    </button>
                </div>
            ) : (
                <p>No user found.</p>
            )}
        </div>
    )
}
