// @ts-check
import { useState } from 'react'
import { Navigate } from 'react-router-dom'
import useAuthService from '../hooks/useAuthService'

export default function SignInForm() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const { signin, user, error } = useAuthService()

    /**
     * @param {React.FormEvent<HTMLFormElement>} e
     */
    async function handleFormSubmit(e) {
        e.preventDefault()
        await signin({ username, password })
    }

    if (user) {
        // console.log('user', user)
        // const url = `/user/${user.id}`
        return <Navigate to="/" />
    }

    return (
        <>
            <form onSubmit={handleFormSubmit}>
                {error && <div className="error">{error.message}</div>}
                <div className="form-floating mb-3 text-center">
                    <input
                        type="text"
                        className="form-control"
                        name="username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        placeholder="Enter Username"
                        style={{ color: 'black' }}
                    />
                </div>
                <div className="form-floating mb-3 text-center">
                    <input
                        type="text"
                        name="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Enter Password"
                        style={{ color: 'black' }}
                    />
                </div>
                <div className="form-floating mb-3 text-center">
                    <button className="bg-yellow-800 text-white px-4 py-2 rounded hover:text-yellow-400">
                        Sign In
                    </button>
                </div>
            </form>
        </>
    )
}
