// @ts-check
import { useState } from 'react'
import { Navigate } from 'react-router-dom'

import useAuthService from '../hooks/useAuthService'

export default function SignUpForm() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [errorMessage, setErrorMessage] = useState('') // State for error messages
    const [signedUpSuccess, setsignedUpSuccess] = useState(false)
    const [redirect, setRedirect] = useState(false)
    const { signup, user } = useAuthService()

    /**
     * @param {React.FormEvent<HTMLFormElement>} e
     */
    async function handleFormSubmit(e) {
        e.preventDefault()
        // Clear previous error messages
        setErrorMessage('')

        // Check if passwords match
        if (password !== confirmPassword) {
            setErrorMessage('Passwords do not match.')
            return
        }
        setsignedUpSuccess(true)
        await signup({ username, password })
        setTimeout(() => {
            setRedirect(true)
        }, 1000)
    }

    if (redirect && user) {
        return <Navigate to="/" />
    }

    return (
        <div className="flex justify-center items-center h-screen ">
            <div className="w-1/2 h-1/2 p-6 shadow-lg bg-white rounded-md">
                <h1 className="text-5xl my-5">Sign Up</h1>
                <form onSubmit={handleFormSubmit}>
                    <input
                        type="text"
                        name="username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        placeholder="Enter Username"
                        className="border w-1/5 h-14 px-2 py-1 "
                    />
                    <input
                        type="password"
                        name="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Enter Password"
                        className="border w-1/5 h-14 px-2 py-1 "
                    />
                    <input
                        type="password"
                        name="confirmPassword"
                        value={confirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                        placeholder="Confirm Password"
                        className="border w-1/5 h-14 px-2 py-1 "
                    />
                    {errorMessage && (
                        <div className="error">{errorMessage}</div>
                    )}
                    {signedUpSuccess && (
                        <div className="success">Signup successful!</div>
                    )}
                    <button type="submit">Sign Up</button>
                </form>
            </div>
        </div>
    )
}
