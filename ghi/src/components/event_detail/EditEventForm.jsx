import { useState, useEffect } from 'react'
import useAuthService from '../../hooks/useAuthService'
import { useParams } from 'react-router-dom'

function EditEventForm() {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [date, setDate] = useState('')
    const [type, setType] = useState('')
    const [capacity, setCapacity] = useState(0)
    const [location, setLocation] = useState('')
    const [successMessage, setSuccessMessage] = useState('')
    const { user } = useAuthService()
    const { event_id } = useParams()

    useEffect(() => {
        async function fetchEventData() {
            try {
                const response = await fetch(
                    `http://localhost:8000/api/events/user/${user.id}/events/${event_id}`,
                    {
                        method: 'GET',
                        credentials: 'include',
                    }
                )
                if (response.ok) {
                    const eventData = await response.json()
                    setName(eventData.name)
                    setDescription(eventData.description)
                    setDate(eventData.date)
                    setType(eventData.type)
                    setCapacity(eventData.capacity)
                    setLocation(eventData.location)
                } else {
                    console.error('Could not fetch event data')
                }
            } catch (error) {
                console.error('Error fetching event data:', error)
            }
        }
        if (user && user.id && event_id) {
            fetchEventData()
        }
    }, [user, event_id])

    async function handleFormSubmit(event) {
        event.preventDefault()
        const data = {
            name,
            description,
            date,
            type,
            capacity,
            location,
        }

        const eventUrl = `http://localhost:8000/api/events/user/${user.id}/events/${event_id}`
        const fetchConfig = {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
            credentials: 'include',
        }

        const response = await fetch(eventUrl, fetchConfig)
        if (response.ok) {
            setSuccessMessage("Your Event Was Edited Successfully")
            console.log('Event updated successfully')
        } else {
            console.error('Could not update the event')
        }
    }

    return (
        <div className="edit-event-form">
            <form onSubmit={handleFormSubmit}>
                <div className="row">
                    <div className="offset-3 col-6">
                        <h1>Edit Event</h1>
                        {successMessage && (
                            <div className="alert alert-success" role="alert">
                                {successMessage}
                            </div>
                        )}
                        <div className="form-floating mb-3">
                            <input
                                type="text"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Event Name"
                                required
                                style={{ color: 'black' }}
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                type="text"
                                value={description}
                                onChange={(e) => setDescription(e.target.value)}
                                placeholder="Event Content"
                                required
                                style={{ color: 'black' }}
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                type="text"
                                value={date}
                                onChange={(e) => setDate(e.target.value)}
                                placeholder="Date of an event"
                                style={{ color: 'black' }}
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                value={type}
                                onChange={(e) => setType(e.target.value)}
                                required
                                className="form-select"
                                style={{ color: 'black' }}
                            >
                                <option value="">Select Event Type</option>
                                <option value="Sport">Sport Events</option>
                                <option value="Music">Music Events</option>
                                <option value="Workshop">Workshops</option>
                                <option value="Social Gathering">Gatherings</option>
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                type="text"
                                value={capacity}
                                onChange={(e) => setCapacity(e.target.value)}
                                placeholder="Event Capacity"
                                style={{ color: 'black' }}
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                type="text"
                                value={location}
                                onChange={(e) => setLocation(e.target.value)}
                                placeholder="Event Location"
                                style={{ color: 'black' }}
                            />
                        </div>
                        <button className="brown-button">
                            Update Event
                        </button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default EditEventForm
