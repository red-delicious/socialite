import { useState, useEffect } from 'react'
// import { useParams } from 'react-router-dom'
import useAuthService from '../../hooks/useAuthService'

export default function AttendeeList({ event_id }) {
    // const { event_id } = useParams()
    const [attendeeList, setAttendeeList] = useState([])
    const { user } = useAuthService()

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(
                `http://localhost:8000/api/user/${user.id}/events/${event_id}/invitations`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response.ok) {
                const data = await response.json()
                setAttendeeList(
                    data.filter((attendee) => attendee.status === 'accepted')
                )
            }
        }
        fetchData()
    }, [user.id, event_id])

    return (
        <>
            <h1 className="flex justify-center px-38 text-yellow-900">Attendee List</h1>
            <div className="m-3 flex justify-items-center px-24">
            <table className="table table-striped ml-auto text-yellow-900">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {attendeeList?.map((attendee) => {
                        return (
                            <tr key={attendee.attendee_id}>
                                <td>{attendee.attendee_username}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            </div>
        </>
    )
}
