import { useEffect, useState } from 'react'
import AttendeeList from './attendee_list'
import { useParams } from 'react-router-dom'
import useAuthService from '../../hooks/useAuthService'

function EventDetail() {
    const [event, setEvent] = useState('')
    const { user } = useAuthService()
    const { event_id } = useParams()

    useEffect(() => {
        const getEventDetail = async () => {
            const response = await fetch(
                `http://localhost:8000/api/events/user/${user.id}/events/${event_id}`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response.ok) {
                const data = await response.json()
                setEvent(data)
            }
        }
        getEventDetail()
    }, [user.id, event_id])
    if (!event) {
        return <div>No event found</div>
    }

    return (
        <div className="max-w-7xl mx-auto px-4 py-8 mt-0 bg-white-400">
            <div className="w-64 h-64">
                <AttendeeList event_id={event_id} />
            </div>
            <div className="max-w-7xl mx-auto px-4 py-8">
                <div className="bg-white rounded-lg shadow p-6 text-center mb-6">
                    <h1 className="text-2xl font-bold text-yellow-800">Event Details</h1>
                    <h2 className="text-lg font-semibold text-yellow-800">
                        {event.name}
                    </h2>
                    <p className="text-gray-600">Date: {event.date}</p>
                    <p className="text-gray-600">Type: {event.type}</p>
                    <p className="text-gray-600">Capacity: {event.capacity}</p>
                    <p className="text-gray-600">Location: {event.location}</p>
                    <p className="text-gray-600">
                        Description: {event.description}
                    </p>
                    <p className="text-gray-600">
                        Hosted by: {event.creator_username}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default EventDetail
