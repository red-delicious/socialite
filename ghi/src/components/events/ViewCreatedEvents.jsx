import { useEffect, useState } from 'react'
import useAuthService from '../../hooks/useAuthService'
import { Link } from 'react-router-dom'

//view all events created by friends
// id is the user_id of the user
function ViewCreatedEvents({ id, username }) {
    const [eventsCreated, seteventsCreated] = useState([])
    const { user } = useAuthService()
    const [reloadData, setReloadData] = useState(false)

    useEffect(() => {
        const getData = async () => {
            //get data of event list
            const response = await fetch(
                `http://localhost:8000/api/events/user/events`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response.ok) {
                const data = await response.json()
                seteventsCreated(data)

                // Filter events created by the user
                const created = data.filter(
                    (event) => event.creator_id === Number(id)
                )
                seteventsCreated(created)
            }
        }
        getData()
    }, [id, reloadData, eventsCreated])

    const handleDeleteEvent = async (event_id) => {
        try {
            const response = await fetch(
                `http://localhost:8000/api/events/user/${id}/events/${event_id}`,
                {
                    method: 'DELETE',
                    credentials: 'include',
                }
            )

            if (response.ok) {
                console.log('Deleted')
                setReloadData(!reloadData)
            } else {
                console.error('Deletion failed')
            }
        } catch (error) {
            console.error('Error Deleting', error)
        }
    }

    return (
        <div className="h-screen  max-w-7xl mx-auto px-4 py-8">
            <h1 className="text-1xl font-bold text-center mb-6 text-yellow-800">
                {username === user.username
                    ? 'Events Created'
                    : `Events ${username} Created`}
            </h1>
            <div className="grid grid-cols-2 gap-1">
                {eventsCreated.map((event) => (
                    <div
                        key={event.id}
                        className="bg-white rounded-lg shadow p-0 flex flex-col justify-between h-96 w-full "
                    >
                        <h2 className="text-3xl  font-semibold text-yellow-800 text-center">
                            {event.name}
                        </h2>
                        <p className="text-gray-600 text-center">Date: {event.date}</p>
                        <p className="text-gray-600 text-center">Type: {event.type}</p>
                        <p className="text-gray-600 text-center">
                            Capacity: {event.capacity} people
                        </p>
                        <p className="text-gray-600 text-center">
                            Location: {event.location}
                        </p>
                        <div className="flex justify-center space-x-2">
                            <Link to={`/attendees/${event.id}`}>
                                <button className="brown-button">
                                    View Details
                                </button>
                            </Link>
                            {user.id == event.creator_id && (
                                <Link to={`/edit/event/${event.id}`}>
                                    <button className="brown-button">
                                        Edit Event
                                    </button>
                                </Link>
                            )}
                        </div>
                        <div className="py-1 flex justify-center">
                            {user.id == event.creator_id && (
                                <button
                                    className="red-button py-2"
                                    onClick={() => handleDeleteEvent(event.id)}
                                >
                                    Delete
                                </button>
                            )}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}
export default ViewCreatedEvents
