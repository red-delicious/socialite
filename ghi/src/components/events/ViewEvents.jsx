import { useEffect, useState } from 'react'
import useAuthService from '../../hooks/useAuthService'
import { Link } from 'react-router-dom'

//view all events created by friends
function ViewEvents() {
    const [friends, setFriends] = useState([])
    const [events, setEvents] = useState([])
    const [friendEvents, setFriendEvents] = useState([])
    const { user } = useAuthService()

    useEffect(() => {
        const getData = async () => {
            //get data of friend list
            const response1 = await fetch(
                `http://localhost:8000/api/user/${user.id}/friends`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response1.ok) {
                const data = await response1.json()
                setFriends(data)
            }

            //get data of event list
            const response2 = await fetch(
                `http://localhost:8000/api/events/user/events`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response2.ok) {
                const data = await response2.json()
                setEvents(data)
            }
        }
        getData()
    }, [user.id])

    useEffect(() => {
        // Filter events whose creator is in friend list
        const filteredEvents = events.filter((event) =>
            friends.some((friend) => friend.friend_id === event.creator_id)
        )
        setFriendEvents(filteredEvents)
    }, [events, friends])

    return (
        <div className="max-w-7xl mx-auto px-4 py-8 h-screen">
            <h1 className="text-4xl text-yellow-800 font-bold text-center mb-6">
                Events Created by My Friends
            </h1>
            <div className="grid grid-cols-3 gap-4">
                {friendEvents.map((event) => (
                    <div
                        key={event.id}
                        className="bg-white rounded-lg shadow p-6"
                    >
                        <h2 className="text-4xl font-semibold text-yellow-800">
                            {event.name}
                        </h2>
                        <p className="text-gray-600">Date: {event.date}</p>
                        <p className="text-gray-600">Type:{event.type}</p>
                        <p className="text-gray-600">
                            This event will include {event.capacity} people
                        </p>
                        <p className="text-gray-600">
                            Location: {event.location}
                        </p>
                        <p className="text-gray-600">
                            Hosted by: {event.creator_username}
                        </p>
                        <Link to={`/attendees/${event.id}`}>
                            <button className="purple-button">
                                View Details
                            </button>
                        </Link>
                    </div>
                ))}
            </div>
        </div>
    )
}
export default ViewEvents
