import { useEffect, useState } from 'react'
import useAuthService from '../../hooks/useAuthService'

//view all joined events
// id is the user_id of the user
function ViewJoinedEvents({ id, username }) {
    const [eventsJoined, seteventsJoined] = useState([])
    //const [filteredEvents, setfilterEvents] = useState([])

    const { user } = useAuthService()

    useEffect(() => {
        const getData = async () => {
            // Fetch invitations
            const response1 = await fetch(
                `http://localhost:8000/api/user/${id}/invitations/accepted`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response1.ok) {
                const invitationsData = await response1.json()
                // Extract host_ids from invitations
                const eventIds = new Set(
                    invitationsData.map((inv) => inv.event_id)
                )

                // Fetch all events
                const response2 = await fetch(
                    `http://localhost:8000/api/events/user/events`,
                    {
                        method: 'GET',
                        credentials: 'include',
                    }
                )
                if (response2.ok) {
                    const eventsData = await response2.json()
                    // Filter events where creator_id is in hostIds
                    const filteredEvents = eventsData.filter((event) =>
                        eventIds.has(event.id)
                    )
                    seteventsJoined(filteredEvents)
                }
            }
        }
        getData()
    }, [id])
    return (
        <div className="h-screen  max-w-7xl mx-auto px-4 py-8">
            <h1 className="text-1xl font-bold text-center mb-6 text-yellow-800">
                {username === user.username
                    ? 'Events I Joined'
                    : `Events ${username} Joined`}
            </h1>
            <div className="grid grid-cols-2 gap-1">
                {eventsJoined.map((event) => (
                    <div
                        key={event.id}
                        className="bg-white rounded-lg shadow p-0 flex flex-col justify-between h-96 w-full"
                    >
                        <h2 className="text-3xl font-semibold text-yellow-800 text-center">
                            {event.name}
                        </h2>
                        <p className="text-gray-600 text-center">Date: {event.date}</p>
                        <p className="text-gray-600 text-center">Type: {event.type}</p>
                        <p className="text-gray-600 text-center">
                            This event will include {event.capacity} people
                        </p>
                        <p className="text-gray-600 text-center">
                            Location: {event.location}
                        </p>
                    </div>
                ))}
            </div>
        </div>
    )
}
export default ViewJoinedEvents
