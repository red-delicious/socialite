// @ts-check
import { useState, useEffect } from 'react'

import useAuthService from '../../hooks/useAuthService'

export default function FriendRequestNotifications() {
    //const [error, setError] = useState(null);
    const [friendRequests, setFriendRequests] = useState([])
    const [acceptSuccessMessage, setacceptSuccessMessage] = useState('') // State to hold the success message
    const [denySuccessMessage, setdenySuccessMessage] = useState('')
    const { user } = useAuthService()

    const getFriendRequests = async () => {
        try {
            const response = await fetch(
                `http://localhost:8000/api/user/${user.id}/requests`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response.ok) {
                const data = await response.json()
                setFriendRequests(data)
            }
        } catch (err) {
            //setError(err.message);
        }
    }

    const handleAccept = async (user_id, friend_id) => {
        try {
            const response = await fetch(
                `http://localhost:8000/api/user/${user.id}/friends/${friend_id}/accept`,
                {
                    method: 'PUT',
                    credentials: 'include',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ user_id, friend_id }),
                }
            )

            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
            setacceptSuccessMessage('Friend request accepted!')
        } catch (error) {
            console.error(
                'There was a problem with the fetch operation:',
                error
            )
        }
    }

    const handleDeny = async (user_id, friend_id) => {
        try {
            const response = await fetch(
                `http://localhost:8000/api/user/${user.id}/friends/${friend_id}`,
                {
                    method: 'DELETE',
                    credentials: 'include',
                }
            )

            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
            setdenySuccessMessage('Friend request denied!')
        } catch (error) {
            console.error(
                'There was a problem with the fetch operation:',
                error
            )
        }
    }

    useEffect(() => {
        getFriendRequests()
        const interval = setInterval(() => {
            getFriendRequests()
        }, 1000)

        return () => clearInterval(interval)
    })

    return (
        <div>
            <h1>Your pending friend requests</h1>
            <ul>
                {friendRequests.map((request) => (
                    <li
                        key={request.sender_id}
                        className="flex space-x-4 items-center mb-2 mx-2"
                    >
                        <div>{request.username} send you a friend request </div>
                        <div className="flex space-x-2">
                            <button
                                onClick={() =>
                                    handleAccept(user.id, request.sender_id)
                                }
                                className="bg-green-500 px-4 py-2 rounded mr-2"
                            >
                                Accept
                            </button>
                            <button
                                onClick={() =>
                                    handleDeny(user.id, request.sender_id)
                                }
                                className="bg-red-500 px-4 py-2 rounded mr-2"
                            >
                                Deny
                            </button>
                        </div>
                    </li>
                ))}
                {acceptSuccessMessage && (
                    <div className="text-green-500">{acceptSuccessMessage}</div>
                )}
                {denySuccessMessage && (
                    <div className="text-red-500">{denySuccessMessage}</div>
                )}
            </ul>
        </div>
    )
}
