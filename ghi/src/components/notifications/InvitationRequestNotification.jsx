// @ts-check
import { useState, useEffect } from 'react'

import useAuthService from '../../hooks/useAuthService'

export default function InvitationRequestNotification() {
    //const [error, setError] = useState(null);
    const [invitations, setInvitations] = useState([])
    const [acceptSuccessMessage, setacceptSuccessMessage] = useState('') // State to hold the success message
    const [denySuccessMessage, setdenySuccessMessage] = useState('')
    const { user } = useAuthService()

    const getInvitations = async () => {
        try {
            const response = await fetch(
                `http://localhost:8000/api/user/${user.id}/invitations`,
                {
                    method: 'GET',
                    credentials: 'include',
                }
            )
            if (response.ok) {
                const data = await response.json()
                setInvitations(data)
            }
        } catch (err) {
            //setError(err.message);
        }
    }

    const handleAccept = async (user_id, friend_id, event_id) => {
        try {
            const response = await fetch(
                `http://localhost:8000/api/user/${user.id}/friends/${friend_id}/events/${event_id}/accept`,
                {
                    method: 'PUT',
                    credentials: 'include',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ user_id, friend_id }),
                }
            )

            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
            setacceptSuccessMessage('Invitation accepted!')
        } catch (error) {
            console.error(
                'There was a problem with the fetch operation:',
                error
            )
        }
    }

    const handleDeny = async (user_id, friend_id, event_id) => {
        try {
            const response = await fetch(
                `http://localhost:8000/api/user/${user.id}/friends/${friend_id}/events/${event_id}/deny`,
                {
                    method: 'PUT',
                    credentials: 'include',
                }
            )

            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
            setdenySuccessMessage('Invitation request denied!')
        } catch (error) {
            console.error(
                'There was a problem with the fetch operation:',
                error
            )
        }
    }

    useEffect(() => {
        getInvitations()
        const interval = setInterval(() => {
            getInvitations()
        }, 1000)

        return () => clearInterval(interval)
    })

    return (
        <div>
            <h1>Your pending event invitations</h1>
            <ul>
                {invitations.map((invitation) => (
                    <li
                        key={invitation.host_id}
                        className="flex space-x-4 items-center mb-2 mx-2"
                    >
                        <div>{invitation.username} send you a invitation </div>
                        <div className="flex space-x-2">
                            <button
                                onClick={() =>
                                    handleAccept(
                                        user.id,
                                        invitation.host_id,
                                        invitation.event_id
                                    )
                                }
                                className="bg-green-500 px-4 py-2 rounded mr-2"
                            >
                                Accept
                            </button>
                            <button
                                onClick={() =>
                                    handleDeny(
                                        user.id,
                                        invitation.host_id,
                                        invitation.event_id
                                    )
                                }
                                className="bg-red-500 px-4 py-2 rounded mr-2"
                            >
                                Deny
                            </button>
                        </div>
                    </li>
                ))}
                {acceptSuccessMessage && (
                    <div className="text-green-500">{acceptSuccessMessage}</div>
                )}
                {denySuccessMessage && (
                    <div className="text-red-500">{denySuccessMessage}</div>
                )}
            </ul>
        </div>
    )
}
