// @ts-check
import FriendRequestNotifications from './FriendRequestNotifications'
import InvitationRequestNotification from './InvitationRequestNotification'

export default function NotificationPage() {
    return (
        <div className="max-w-7xl mx-auto px-4 py-8">
            <h1 className="text-3xl font-bold text-center mb-8">
                Notifications
            </h1>
            <div className="grid grid-cols-1 gap-6">
                <FriendRequestNotifications />
                <InvitationRequestNotification />
            </div>
        </div>
    )
}
