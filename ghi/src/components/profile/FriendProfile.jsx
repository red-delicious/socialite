import ViewCreatedEvents from '../events/ViewCreatedEvents'
import ViewJoinedEvents from '../events/ViewJoinedEvents'

import { useEffect, useState } from 'react'

function FriendProfile({ id }) {
    const [friend, setFriend] = useState([])

    useEffect(() => {
        const getData = async () => {
            //get data of user list
            const response = await fetch(`http://localhost:8000/api/user/all`, {
                method: 'GET',
                credentials: 'include',
            })
            if (response.ok) {
                const data = await response.json()

                // Filter user created by the user id
                const friend = data.filter((friend) => friend.id === Number(id))
                setFriend(friend)
            }
        }
        getData()
    }, [id])

    return (
        <div className="w-full h-screen mx-auto px-12 py-8 flex">
            <div className=" flex-3 flex w-full">
                <h1 className="flex-[3] px-0 ">
                    Welcome to {friend[0]?.username} profile
                </h1>
                <div className="flex-[3] h-screen">
                    <ViewCreatedEvents
                        id={friend[0]?.id}
                        username={friend[0]?.username}
                    />
                </div>
                <div className="flex-[3] h-screen">
                    <ViewJoinedEvents id={id} username={friend[0]?.username} />
                </div>
            </div>
        </div>
    )
}
export default FriendProfile
