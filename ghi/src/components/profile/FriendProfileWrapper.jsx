// FriendProfileWrapper.jsx
import { useParams } from 'react-router-dom'
import FriendProfile from './FriendProfile' // Adjust the path as necessary

function FriendProfileWrapper() {
    const { user_id } = useParams()
    return <FriendProfile id={user_id} />
    //return <div>111</div>
}
export default FriendProfileWrapper
