import FriendList from '../FriendList'
import ViewCreatedEvents from '../events/ViewCreatedEvents'
import ViewJoinedEvents from '../events/ViewJoinedEvents'
import useAuthService from '../../hooks/useAuthService'
import { Link } from 'react-router-dom'

function MyProfile() {
    const { user } = useAuthService()
    return (
        <div className="w-full h-screen mx-auto px-12 py-8 flex">
            <div className=" flex-3 flex w-full">
                <div className="flex-[3]">
                    <FriendList />
                </div>
                <div className="flex-[3] h-screen">
                    <ViewCreatedEvents id={user.id} username={user.username} />
                </div>
                <div className="flex-[3] h-screen">
                    <ViewJoinedEvents id={user.id} username={user.username} />
                </div>
                <div className="flex-[1] h-screen">
                    <Link to={`/invitefriend`}>
                        <button className="bg-yellow-700 text-white text-sm font-semibold rounded-full px-2 py-2 hover:bg-yellow-600 transition duration-300 ease-in-out;">
                            Invite Friend
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    )
}
export default MyProfile
