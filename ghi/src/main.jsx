//@ts-check
import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import { Navigate } from 'react-router-dom'
import useAuthService from './hooks/useAuthService'

import SignInForm from './components/SignInForm'
import SignUpForm from './components/SignUpForm'
import App from './App'
import AuthProvider from './components/AuthProvider'
import NotificationPage from './components/notifications/NotificationPage'
import FriendList from './components/FriendList'
import SearchForUser from './components/SearchForUser'
import CreateEventForm from './components/CreateEventForm'
import ViewEvents from './components/events/ViewEvents'
import EventDetail from './components/event_detail/event_detail'
import MyProfile from './components/profile/MyProfile'
import GuestPage from './components/GuestPage'
import './index.css'
import InviteFriendForm from './components/InviteFriendForm'
import FriendProfileWrapper from './components/profile/FriendProfileWrapper'
import EditEventForm from './components/event_detail/EditEventForm'

const BASE_URL = import.meta.env.BASE_URL
if (!BASE_URL) {
    throw new Error('BASE_URL is not defined')
}

const ProtectedRoute = ({ children }) => {
    const { isLoggedIn } = useAuthService()
    if (!isLoggedIn) {
        return <Navigate to="/guest" replace />
    }
    return children
}
export default ProtectedRoute

const router = createBrowserRouter(
    [
        {
            path: '/',
            element: (
                <ProtectedRoute>
                    <App />
                </ProtectedRoute>
            ),
            children: [
                {
                    index: true,
                    element: <ViewEvents />,
                },
                {
                    path: '/notifications',
                    element: <NotificationPage />,
                },
                {
                    path: '/friendlist',
                    element: <FriendList />,
                },
                {
                    path: '/searchforuser',
                    element: <SearchForUser />,
                },
                {
                    path: '/createevent',
                    element: <CreateEventForm />,
                },
                {
                    path: '/myprofile',
                    element: <MyProfile />,
                },
                {
                    path: '/attendees/:event_id',
                    element: <EventDetail />,
                },
                {
                    path: '/edit/event/:event_id',
                    element: <EditEventForm />,
                },
                {
                    path: '/invitefriend',
                    element: <InviteFriendForm />,
                },
                {
                    path: '/friendprofile/:user_id',
                    element: <FriendProfileWrapper />,
                },
            ],
        },
        {
            path: '/guest',
            element: <GuestPage />,
        },
        {
            path: 'signin',
            element: <SignInForm />,
        },
        {
            path: 'signup',
            element: <SignUpForm />,
        },
    ],
    {
        basename: BASE_URL,
    }
)

const rootElement = document.getElementById('root')
if (!rootElement) {
    throw new Error('root element was not found!')
}

// Log out the environment variables while you are developing and deploying
// This will help debug things
console.table(import.meta.env)

const root = ReactDOM.createRoot(rootElement)
root.render(
    <React.StrictMode>
        <AuthProvider>
            <RouterProvider router={router} />
        </AuthProvider>
    </React.StrictMode>
)
