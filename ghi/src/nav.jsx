import { useState } from 'react'
import { NavLink } from 'react-router-dom'
import useAuthService from './hooks/useAuthService'
import SearchForUser from './components/SearchForUser'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import IconButton from '@mui/material/IconButton'
import Button from '@mui/material/Button'
import NotificationsIcon from '@mui/icons-material/Notifications'
import CssBaseline from '@mui/material/CssBaseline'
import Box from '@mui/material/Box'
import SearchIcon from '@mui/icons-material/Search'
import logo from '../public/logo.png';

function Nav() {
    const { signout, user } = useAuthService()
    const [showSearch, setShowSearch] = useState(false)


    const toggleSearchBar = () => {
        setShowSearch(!showSearch)
    }

    const handleSignOut = async () => {
        try {
            await signout()
            // navigation redirect to signin
        } catch (error) {
            console.error('Error', error)
        }
    }


    return (
        <>
            <CssBaseline />
            <Box sx={styles.headerContainer}>
                <Typography variant="h2" sx={styles.header}>
                    Socialite
                </Typography>
            </Box>
            <AppBar position="static" sx={styles.appBar}>
                <Toolbar sx={styles.toolbar}>
                    <IconButton edge="start" color="inherit" aria-label="menu" sx={styles.menuButton}>
                        <img src={logo} alt="Logo" style={{ height: 60, marginRight: 10 }} />
                    </IconButton>
                    <Typography variant="body1" sx={styles.welcome}>
                        Welcome {user.username}!
                    </Typography>
                    <Button color="inherit" component={NavLink} to="/" sx={styles.navLink}>
                        Home
                    </Button>
                    <Button color="inherit" component={NavLink} to="/myprofile" sx={styles.navLink}>
                        Profile
                    </Button>
                    <Button color="inherit" component={NavLink} to="/createevent" sx={styles.navLink}>
                        Create an Event
                    </Button>
                    <IconButton color="inherit" onClick={toggleSearchBar} sx={styles.navLink}>
                        <SearchIcon />
                    </IconButton>
                    {showSearch && <SearchForUser />}
                    <Button color="inherit" component={NavLink} to="/notifications" sx={styles.navLink}>
                        <NotificationsIcon />
                    </Button>
                    <Button color="inherit" onClick={handleSignOut} sx={styles.navLink}>
                        Sign Out
                    </Button>
                </Toolbar>
            </AppBar>
        </>
    )
}

const styles = {
    headerContainer: {
        width: '100%',
        textAlign: 'center',
        background: 'linear-gradient(to right, #e0c1a0, #826d58)',
        padding: '20px 0',
        margin: 0,
    },
    header: {
        color: 'white',
        fontFamily: 'Arial, sans-serif',
        fontWeight: 'bold',
        margin: 0,
    },
    appBar: {
        backgroundImage: 'linear-gradient(to right, #e0c1a0, #826d58)',
        width: '100%',
        marginTop: '0px', // Remove or adjust margin as needed
        marginBottom: '0px'
    },
    toolbar: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
        paddingLeft: 0,
        paddingRight: 0,
    },
    menuButton: {
        marginRight: '16px',
    },
    welcome: {
        marginRight: '16px',
    },
    navLink: {
        marginRight: '16px',
        border: 0,
    },
}

export default Nav
