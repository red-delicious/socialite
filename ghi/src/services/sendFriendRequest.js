// friendService.js

/**
 * Function to send a friend request.
 * @param {int} idof
 * @returns {Promise<void>} A promise that resolves if the request was successful.
 */
export async function sendFriendRequest(user_id, friend_id) {
    try {
        const response = await fetch(`http://localhost:8000/api/user/${user_id}/friends/${friend_id}/request`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
        });

        if (response.ok) {
            console.log("Friend request sent!");
        } else {
            throw new Error("Failed to send friend request.");
        }
    } catch (error) {
        console.error("Error sending friend request:", error);
        throw error; // Rethrow the error if you want to handle it in the component
    }
}
