CREATE TABLE "follows" (
  "following_user_id" integer,
  "followed_user_id" integer,
  "created_at" timestamp
);

CREATE TABLE "users" (
  "id" integer PRIMARY KEY,
  "username" varchar UNIQUE,
  "email" varchar,
  "password" varchar
);

CREATE TABLE "events" (
  "id" integer PRIMARY KEY,
  "name" varchar,
  "description" text,
  "creator_id" integer REFERENCES "users" ("id"),
  "date" timestamp,
  "type" varchar,
  "location" varchar,
  ADD FOREIGN KEY ("creator_id") REFERENCES "users" ("id");
);

CREATE TABLE "eventsAttendees" (
  "event_id" integer PRIMARY KEY,
  "user_id" integer
);

COMMENT ON COLUMN "events"."description" IS 'Content of the post';

ALTER TABLE "events" ADD FOREIGN KEY ("creator_id") REFERENCES "users" ("id");

ALTER TABLE "follows" ADD FOREIGN KEY ("following_user_id") REFERENCES "users" ("id");

ALTER TABLE "follows" ADD FOREIGN KEY ("followed_user_id") REFERENCES "users" ("id");
